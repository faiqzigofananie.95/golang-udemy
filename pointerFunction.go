package main

import "fmt"

func main() {
	var alamat = Address{
		City:     "Subang",
		Province: "Jawa Barat",
		Country:  "",
	}

	changeAddressCountryToIndonesia(&alamat)
	fmt.Println(alamat)
	alamat.changeAddressCountryToMalaysia()
	fmt.Println(alamat)
}

type Address struct {
	City, Province, Country string
}

func changeAddressCountryToIndonesia(address *Address) {
	address.Country = "Indonesia"
}

func (address *Address) changeAddressCountryToMalaysia() {
	address.Country = "Malaysia"
}

package main

import "fmt"

func main() {
	//Pass By Value
	address1 := Address{"Subang", "Jawa Barat", "Indonesia"}
	address2 := address1

	address2.City = "Bandung"
	// fmt.Println(address1)
	// fmt.Println(address2)

	//Pass By Reference
	//Operator &
	address3 := Address{"Subang", "Jawa Barat", "Indonesia"}
	address4 := &address3

	address4.City = "Bandung"
	// fmt.Println(address3)
	// fmt.Println(address4)

	address5 := Address{"Subang", "Jawa Barat", "Indonesia"}
	address6 := &address5

	address6.City = "Bandung"
	address6 = &Address{"Jakarta Barat", "DKI Jakarta", "Indonesia"}
	// fmt.Println(address5)
	// fmt.Println(address6)

	//Operator *
	address7 := Address{"Subang", "Jawa Barat", "Indonesia"}
	address8 := &address7
	// address9 := &address7

	address8.City = "Bandung"
	*address8 = Address{"Jakarta Barat", "DKI Jakarta", "Indonesia"}
	// fmt.Println(address7)
	// fmt.Println(address8)
	// fmt.Println(address9)
}

type Address struct {
	City, Province, Country string
}

package main

import (
	"fmt"
	"strings"
)

func main() {
	str := []string{"A", "B", "A", "C", "D", "A", "E", "Z", "D"}
	resultArr := []string{}

	for i := range str {
		if i == 0 {
			resultArr = append(resultArr, string(str[i]))
		} else if len(resultArr) < 5 {
			for j := 0; j < len(resultArr); j++ {
				if string(str[i]) != string(resultArr[j]) {
					if j < len(resultArr)-1 {
						continue
					}

					resultArr = append(resultArr, string(str[i]))
					fmt.Println(resultArr)
					break
				} else {
					fmt.Println(j)
					resultArr = append(resultArr[:j], resultArr[j+1:]...)
					resultArr = append(resultArr, string(str[i]))
					fmt.Println(resultArr)
					break
				}
			} 
		} else {
			fmt.Println(resultArr)
			for j := range resultArr {
				if string(str[i]) != string(resultArr[j]) {
					if j < len(resultArr)-1 {
						continue
					}
					resultArr = append(resultArr[:0], resultArr[1:]...)
					resultArr = append(resultArr, string(str[i]))
					break
				} else {
					resultArr = append(resultArr[:j], resultArr[j+1:]...)
					resultArr = append(resultArr, string(str[i]))
					break
				}
			} 
		}
	}

	fmt.Println(strings.Join(resultArr, "-"))
}
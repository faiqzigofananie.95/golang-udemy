package main

import "fmt"

func main() {
	var faiq Person
	faiq.Name = "Faiq"
	sayHello(faiq)

	cat := Person{
		Name: "Kucing",
	}
	sayHello(cat)

	fmt.Println(ups(1))
	fmt.Println(ups(2))
	fmt.Println(ups(3))
	fmt.Println(hoyya(1))
	fmt.Println(hoyya("Faiq"))
	fmt.Println(hoyya(true))
}

//Interface
type HasName interface {
	GetName() string
}

func sayHello(hasName HasName) {
	fmt.Println("Hello", hasName.GetName())
}

type Person struct {
	Name string
}

func (person Person) GetName() string {
	return person.Name
}

type Animal struct {
	Name string
}

func (animal Animal) GetName() string {
	return animal.Name
}

//Interface Kosong
func ups(i int) interface{} {
	if i == 1 {
		return 1
	} else if i == 2 {
		return "ini 2"
	} else {
		return false
	}
}

func hoyya(i interface{}) interface{} {
	return i
}

package main

import "fmt"

func main() {
	//struct
	type strc struct {
		Name string
		Age  int
	}
	////////////////////
	var faiq strc
	faiq.Name = "Faiq Zigo Fananie"
	faiq.Age = 23
	////////////////////
	bocil := strc{
		Name: "Nandita Naadaasa",
		Age:  20,
	}
	////////////////////
	fmt.Println(faiq)
	fmt.Println(bocil)
}

package main

import "fmt"

func main() {
	var faiq Customer
	faiq.Name = "Faiq Zigo Fananie"
	faiq.Address = "jatijajar"
	faiq.Age = 24

	fmt.Println(faiq)
	fmt.Println(faiq.Name)
	fmt.Println(faiq.Address)
	fmt.Println(faiq.Age)

	nandita := Customer{
		Name:    "Nandita Naadaasa",
		Address: "Jatijajar",
		Age:     22,
	}

	fmt.Println(nandita)
	fmt.Println(nandita.Name)
	fmt.Println(nandita.Address)
	fmt.Println(nandita.Age)

	rully := Customer{
		Name: "Rully",
	}
	sayHi(rully, "faiq")
	rully.sayHello("faiq")
}

type Customer struct {
	Name, Address string
	Age           int
}

func sayHi(customer Customer, name string) {
	fmt.Println("Hello, my name is", customer.Name+". Saya teman dari", name)
}

func (customer Customer) sayHello(name string) {
	fmt.Println("Hello, my name is", customer.Name+". Saya teman dari", name)
}

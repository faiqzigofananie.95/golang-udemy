package json_udemy

import (
	"encoding/json"
	"fmt"
	"testing"
)

func logJson(data interface{}) {
	bytes, err := json.Marshal(data)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(bytes))
}

func TestEncode(t *testing.T) {
	logJson("Faiq")
	logJson(1)
	logJson(true)
	logJson([]string {"Faiq", "Zigo", "Fananie"})
}

type Address struct {
	Street	string
	Country	string
	Postalcode	string
}

type Customer struct {
	Firstname	string
	Middlename	string
	Lastname	string
	Age	int
	Married	bool
	Hobbies	[]string
	Addresses []Address
}

func TestEncodeObject(t *testing.T) {
	customer := Customer{
		Firstname: "Faiq",
		Middlename: "Zigo",
		Lastname: "Fananie",
		Age: 24,
		Married: false,
		Hobbies: []string{"main", "futsal"},
		Addresses: []Address{
			{
				Street: "Jatijajar",
				Country: "Indonesia",
				Postalcode: "16466",
			},
			{
				Street: "Hl. Toran 88B",
				Country: "Indonesia",
				Postalcode: "99999",
			},
		},
	}

	bytes, err := json.Marshal(customer)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(bytes))
}
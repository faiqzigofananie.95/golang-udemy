package json_udemy

import (
	"encoding/json"
	"fmt"
	"testing"
)

func TestDecode(t *testing.T) {
	jsonString := `{"Firstname":"Faiq","Middlename":"Zigo","Lastname":"Fananie","Age":24,"Married":false,"Hobbies":["main","futsal"],"Addresses":[{"Street":"Jatijajar","Country":"Indonesia","Postalcode":"16466"},{"Street":"Hl. Toran 88B","Country":"Indonesia","Postalcode":"99999"}]}`
	jsonBytes := []byte(jsonString)

	customer := &Customer{}

	err := json.Unmarshal(jsonBytes, customer)
	if err != nil {
		panic(err)
	} 

	fmt.Println(customer)
	fmt.Println(customer.Firstname)
	fmt.Println(customer.Middlename)
	fmt.Println(customer.Lastname)
	fmt.Println(customer.Hobbies)
	fmt.Println(customer.Addresses)
	fmt.Println(customer.Addresses[1].Street)
}

func TestDecodeOnlyJsonArray(t *testing.T) {
	jsonString := `[{"Street":"Jatijajar","Country":"Indonesia","Postalcode":"16466"},{"Street":"Hl. Toran 88B","Country":"Indonesia","Postalcode":"99999"}]`
	jsonBytes := []byte(jsonString)

	addresses := &[]Address{}

	err := json.Unmarshal(jsonBytes, addresses)
	if err != nil {
		panic(err)
	} 

	fmt.Println(addresses)
}
package json_udemy

import (
	"encoding/json"
	"fmt"
	"testing"
)

type Product struct {
	Id	string	`json:"id"`
	Name	string	`json:"name"`
	ImageURL	string	`json:"image_url"`
}

func TestJsonTagEncode(t *testing.T) {
	product := Product{
		Id:	"P0001",
		Name: "Ubuntu Linux",
		ImageURL: "http://example.com/image.png",
	}

	bytes, _ := json.Marshal(product)
	fmt.Println(string(bytes))
}

func TestJsonTagDecode(t *testing.T) {
	jsonString := `{"id":"P0001","name":"Ubuntu Linux","image_url":"http://example.com/image.png"}`
	jsonBytes := []byte(jsonString)

	product := &Product{}

	json.Unmarshal(jsonBytes, product)
	fmt.Println(product)
	fmt.Println(product.Name)
}
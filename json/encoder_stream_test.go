package json_udemy

import (
	"encoding/json"
	"os"
	"testing"
)

func TestStreamEncoder(t *testing.T) {
	writer, _ := os.Create("customerOut.json")
	encoder := json.NewEncoder(writer)

	customer := &Customer{
		Firstname: "Faiq",
		Middlename: "Zigo",
		Lastname: "Fananie",
	}

	encoder.Encode(customer)
}

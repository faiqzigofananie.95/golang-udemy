package main

import (
	"fmt"
	"strings"
)

func main() {
		fmt.Println(strings.Contains("Faiq Zigo Fananie", "Faiq"))
		fmt.Println(strings.Split("Faiq Zigo Fananie", " "))
		fmt.Println(strings.ToLower("Faiq Zigo Fananie"))
		fmt.Println(strings.ToUpper("Faiq Zigo Fananie"))
		fmt.Println(strings.Trim("   Faiq Zigo Fananie    ", " "))
		fmt.Println(strings.ReplaceAll("Faiq Zigo Fananie", "Faiq", "Nandita"))
}

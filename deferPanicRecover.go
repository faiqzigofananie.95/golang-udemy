package main

import "fmt"

func main() {
	runApplication(5)
	// runApplication(0)
	runApp(false)
	// runApp(true)
	runAppRecover(true)
}

//Defer
func logging() {
	fmt.Println("Selesai memanggil fungsi")
}

func runApplication(value int) {
	defer logging()
	fmt.Println("Run Application")
	result := 10 / value
	fmt.Println("Result", result)
}

//Panic
func endApp() {
	fmt.Println("Aplikasi selesai")
}

func runApp(error bool) {
	defer endApp()
	if error {
		panic("Aplikasi error")
	}

	fmt.Println("Aplikasi berjalan")
}

//Recover

func endApp2() {
	message := recover()
	if message != nil {
		fmt.Println("Error dengan message", message)
	}
	fmt.Println("Aplikasi selesai")
}

func runAppRecover(error bool) {
	defer endApp2()
	if error {
		panic("Aplikasi error")
	}

	fmt.Println("Aplikasi berjalan")
}

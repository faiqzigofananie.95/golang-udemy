package helper

import (
	"testing"
	"runtime"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestHelloWorld(t *testing.T) {
	result := helloWorld("Faiq")
	assert.Equal(t, "Hello Faiq", result)
}

func TestSkip(t *testing.T) {
	if runtime.GOOS == "darwin" {
		t.Skip("Cannot run on mac os")
	}

	result := helloWorld("Faiq")
	assert.Equal(t, "Hello Faiq", result)
}

func TestMain(m *testing.M) {
	//before
	fmt.Println("this is before")

	m.Run()

	//after
	fmt.Println("this is after")
}

func TestSubTest(t *testing.T) {
	t.Run("Hello World Faiq", func(t *testing.T) {
		result := helloWorld("Faiq")
		assert.Equal(t, "Hello Faiq", result)
	})
	
	t.Run("Hello World Nandita", func(t *testing.T) {
		result := helloWorld("Nandita")
		assert.Equal(t, "Hello Nandita", result)
	})
}

func TestTableTest(t *testing.T) {
	tests := []struct {
		name		string
		request		string
		expected	string
	}{
		{
			name:		"Faiq",
			request:	"Faiq",
			expected:	"Hello Faiq",
		}, {
			name:		"Nandita",
			request:	"Nandita",
			expected:	"Hello Nandita",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := helloWorld(test.request)
			require.Equal(t, test.expected, result)
		})
	}
}

func BenchmarkHelloWorldFaiq(b *testing.B) {
	for i := 0; i < b.N; i++ {
		helloWorld("Faiq")
	}
}

func BenchmarkHelloWorldNandita(b *testing.B) {
	for i := 0; i < b.N; i++ {
		helloWorld("Nandita")
	}
}

func BenchmarkSubHelloWorld(b *testing.B) {
	b.Run("Hello World Faiq", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			helloWorld("Faiq")
		}
	})
	
	b.Run("Hello World Nandita", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			helloWorld("Nandita")
		}
	})
}

func BenchmarkTableHelloWorld(b *testing.B) {
	tests := []struct {
		name		string
		request		string
	}{
		{
			name:		"Faiq",
			request:	"Faiq",
		}, {
			name:		"Nandita",
			request:	"Nandita",
		},
	}

	for _, test := range tests {
		b.Run(test.name, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				helloWorld(test.request)
			}
		})
	}
}
package main

import "fmt"

func main() {
	sayHello()

	sayHelloTo("faiq", "fananie")

	result := getHello("faiq")
	fmt.Println(result)

	firstName, lastName := getFullName()
	fmt.Println(firstName, lastName)
	firstName2, _ := getFullName()
	fmt.Println(firstName2)

	total := sumAll(1, 2, 3)
	fmt.Println(total)
	numbers := []int{1, 2, 3}
	total = sumAll(numbers...)
	fmt.Println(total)

	sayGoodBye := getGoodBye("faiq")
	fmt.Println(sayGoodBye)
	sayGoodBye2 := getGoodBye
	fmt.Println(sayGoodBye2("faiq"))
	sayGoodBye3 := sayGoodBye2("faiq")
	fmt.Println(sayGoodBye3)

	sayHelloWithFilter("Faiq", spamFilter)
	filter := spamFilter
	sayHelloWithFilter("Anjing", filter)
	sayHelloWithFilter2("Faiq", filter)

	blacklist := func(name string) bool {
		return name == "admin"
	}
	registerUser("faiq", blacklist)
	registerUser("admin", func(name string) bool {
		return name == "admin"
	})

	loop := factorialLoop(5)
	fmt.Println(loop)
	recursiveLoop := factorialRecursive(5)
	fmt.Println(recursiveLoop)
}

func sayHello() {
	fmt.Println("hello")
}

func sayHelloTo(firstName string, lastName string) {
	fmt.Println("hello", firstName, lastName)
}

func getHello(name string) string {
	return "hello " + name
}

func getFullName() (string, string) {
	return "faiq", "fananie"
}

func sumAll(numbers ...int) int {
	total := 0
	for _, value := range numbers {
		total += value
	}
	return total
}

func getGoodBye(name string) string {
	return "Good Bye " + name
}

func sayHelloWithFilter(name string, filter func(string) string) {
	nameFiltered := filter(name)
	fmt.Println("Hello", nameFiltered)
}

type Filter func(string) string

func sayHelloWithFilter2(name string, filter Filter) {
	nameFiltered := filter(name)
	fmt.Println("Hello", nameFiltered)
}

func spamFilter(name string) string {
	if name == "Anjing" {
		return "..."
	} else {
		return name
	}
}

type Blacklist func(string) bool

func registerUser(name string, blacklist Blacklist) {
	if blacklist(name) {
		fmt.Println("You are Blocked", name)
	} else {
		fmt.Println("Welcome", name)
	}
}

func factorialLoop(value int) int {
	result := 1
	for i := value; i > 0; i-- {
		result *= i
	}
	return result
}

func factorialRecursive(value int) int {
	if value == 1 {
		return 1
	} else {
		return value * factorialRecursive(value-1)
	}
}

package webserver_udemy

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func SayHello(writer http.ResponseWriter, request *http.Request) {
	name := request.URL.Query().Get("name")
	if name == "" {
		fmt.Fprint(writer, "Hello")
	} else {
		fmt.Fprintf(writer, "Hello %s", name)
	}
}

func TestQuery(t *testing.T) {
	request := httptest.NewRequest("GET", "http://localhost:1234/hello?name=faiq", nil)
	recorder := httptest.NewRecorder()

	SayHello(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)

	fmt.Println(string(body))
}

func MultipleQuery(writer http.ResponseWriter, request *http.Request) {
	firstname := request.URL.Query().Get("firstname")
	lastname := request.URL.Query().Get("lastname")

	fmt.Fprintf(writer, "Hello %s %s", firstname, lastname)
}

func TestMultipleQuery(t *testing.T) {
	request := httptest.NewRequest("GET", "http://localhost:1234/hello?firstname=faiq&lastname=fananie", nil)
	recorder := httptest.NewRecorder()

	MultipleQuery(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)

	fmt.Println(string(body))
}

func MultipleQueryValues(writer http.ResponseWriter, request *http.Request) {
	query := request.URL.Query()
	names := query["name"]

	fmt.Fprint(writer, strings.Join(names, " "))
}

func TestMultipleQueryValues(t *testing.T) {
	request := httptest.NewRequest("GET", "http://localhost:1234/hello?name=faiq&name=fananie", nil)
	recorder := httptest.NewRecorder()

	MultipleQueryValues(recorder, request)

	response := recorder.Result()
	body, _ := io.ReadAll(response.Body)

	fmt.Println(string(body))
}

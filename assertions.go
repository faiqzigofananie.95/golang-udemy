package main

import "fmt"

// Merubah tipe data pada interface kosong menjadi string || bool || int || etc...
func main() {
	result := random()
	fmt.Println(result)
	resultString := result.(string)
	fmt.Println(resultString)

	//Assertions with Switch
	switch resultInt := result.(type) {
	case string:
		fmt.Println("Value", resultInt, "is string")
	case bool:
		fmt.Println("Value", resultInt, "is boolean")
	default:
		fmt.Println("Unknown Type")
	}
}

//Assertions
func random() interface{} {
	return "OK"
}
